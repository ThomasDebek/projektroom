json.extract! admin_gallery, :id, :name, :created_at, :updated_at
json.url admin_gallery_url(admin_gallery, format: :json)
